package com.vmmontes.api

import com.vmmontes.api.kernel.BASE_URL
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class RetrofitService {
    val service: BaseAppApiService by lazy {
        val httpClient = OkHttpClient.Builder().build()
        val GsonConverterFactory = GsonConverterFactory.create()

        val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
            .client(httpClient)
            .addConverterFactory(GsonConverterFactory)
            .build()

        retrofit.create(BaseAppApiService::class.java)
    }
}