package com.vmmontes.api.client

import com.vmmontes.api.model.movie.MoviesResponseModel

interface MoviesClient {

    fun getMovies(language: String, movieName: String): MoviesResponseModel
}