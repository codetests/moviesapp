package com.vmmontes.api.client

import com.vmmontes.api.RetrofitService
import com.vmmontes.api.kernel.API_KEY
import com.vmmontes.api.model.movie.MoviesApiResponseModel
import com.vmmontes.api.model.movie.MoviesResponseModel
import retrofit2.Response

class MoviesClientImp(val retrofitService: RetrofitService): MoviesClient {

    companion object {
        private const val UNKNOW_MAX_PAGES = -1
        private const val INIT_PAGE = 0
    }

    private var currentPage: Int = INIT_PAGE
    private var maxPage: Int = UNKNOW_MAX_PAGES
    private var lastMovieQuery = ""

    override fun getMovies(language: String, movieName: String): MoviesResponseModel {
        var moviesListApiResponseModel = MoviesResponseModel(null, MoviesResponseModel.ResponseState.GENERIC_ERROR)
        try {
            validateAndUpdateManagementAPIValues(movieName)
            val nextPage = this.currentPage + 1
            if (canDoRequestToNextPage(nextPage)) {
                val response = getMoviesListResponse(language, nextPage)
                moviesListApiResponseModel = generateMoviesResponseModelAndUpdateVarPages(nextPage, response)

            } else {
                moviesListApiResponseModel.responseState = MoviesResponseModel.ResponseState.MAX_PAGES
            }
        } catch (e: Exception) {
            resetPages()
            moviesListApiResponseModel.responseState = MoviesResponseModel.ResponseState.GENERIC_ERROR
        }

        return moviesListApiResponseModel
    }

    private fun getMoviesListResponse(language: String, nextPage: Int): Response<MoviesApiResponseModel> {
        val endpoint = retrofitService.service
            .getFindMoviesList(API_KEY, language, lastMovieQuery, nextPage.toString())
        return endpoint.execute()
    }

    private fun generateMoviesResponseModelAndUpdateVarPages(nextPage: Int, response: Response<MoviesApiResponseModel>): MoviesResponseModel {
        var responseState = MoviesResponseModel.ResponseState.GENERIC_ERROR
        var reseponseApiResponseModel: MoviesApiResponseModel? = null
        if (response.isSuccessful) {
            if (response.body() != null) {
                this.currentPage = nextPage
                response.body()?.apply {
                    maxPage = this.totalPages
                    reseponseApiResponseModel = this
                    responseState = MoviesResponseModel.ResponseState.SUCCESS
                }
            } else {
                responseState = MoviesResponseModel.ResponseState.SERVER_ERROR
            }
        } else {
            resetPages()
            responseState = MoviesResponseModel.ResponseState.SERVER_ERROR
        }

        return  MoviesResponseModel(reseponseApiResponseModel, responseState)
    }

    private fun isDifferentMovie(newMovieQuery: String): Boolean = (newMovieQuery != this.lastMovieQuery)

    private fun canDoRequestToNextPage(newPage: Int): Boolean = (this.maxPage == UNKNOW_MAX_PAGES || newPage <= this.maxPage)

    private fun validateAndUpdateManagementAPIValues(movieName: String) {
        if (isDifferentMovie(movieName)) {
            resetPages()
            this.lastMovieQuery = movieName
        }
    }

    private fun resetPages() {
        this.currentPage = INIT_PAGE
        this.maxPage = UNKNOW_MAX_PAGES
    }
}