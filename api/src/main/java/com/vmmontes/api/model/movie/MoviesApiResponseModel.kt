package com.vmmontes.api.model.movie

import com.google.gson.annotations.SerializedName

data class MoviesApiResponseModel(
    @SerializedName("page") val page: Int,
    @SerializedName("results") val results: MutableList<MovieApiModel>,
    @SerializedName("total_results") val totalResults: Int,
    @SerializedName("total_pages") val totalPages: Int
)