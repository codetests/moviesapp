package com.vmmontes.moviesapp.data.datasource.cloud

import com.vmmontes.api.client.MoviesClient
import com.vmmontes.moviesapp.data.mapper.MovieMapper
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel

class MoviesCloudDataSourceImp(val moviesClient: MoviesClient): MoviesCloudDataSource {

    override fun getMovies(language: String, movieName: String): ListMovieDomainModel {
        val response = moviesClient.getMovies(language, movieName)
        return MovieMapper.moviesListToDomain(response)
    }
}