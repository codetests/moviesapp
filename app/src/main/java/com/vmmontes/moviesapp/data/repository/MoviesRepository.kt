package com.vmmontes.moviesapp.data.repository

import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

interface MoviesRepository {
    fun getMovies(language: String, movieName: String): ListMovieDomainModel
    fun getStoredList(): MutableList<MovieDomainModel>
    fun getMovie(id: Int): MovieDomainModel?
    fun addMoviesIntoStoredList(moviesList: MutableList<MovieDomainModel>)
    fun cleanStoredList()
}