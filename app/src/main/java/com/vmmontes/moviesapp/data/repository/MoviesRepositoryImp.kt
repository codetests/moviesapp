package com.vmmontes.moviesapp.data.repository

import com.vmmontes.moviesapp.data.datasource.cloud.MoviesCloudDataSource
import com.vmmontes.moviesapp.data.datasource.cloud.MoviesCloudDataSourceImp
import com.vmmontes.moviesapp.data.datasource.local.MoviesLocalDataSource
import com.vmmontes.moviesapp.data.datasource.local.MoviesLocalDataSourceImp
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class MoviesRepositoryImp(val moviesCloudDataSource: MoviesCloudDataSource,
                          val moviesLocalDataSource: MoviesLocalDataSource): MoviesRepository {

    override fun getMovies(language: String, movieName: String): ListMovieDomainModel = moviesCloudDataSource.getMovies(language, movieName)

    override fun getMovie(id: Int): MovieDomainModel? = moviesLocalDataSource.getMovie(id)

    override fun getStoredList(): MutableList<MovieDomainModel> = moviesLocalDataSource.getList()

    override fun addMoviesIntoStoredList(moviesList: MutableList<MovieDomainModel>) {
        moviesLocalDataSource.addall(moviesList)
    }

    override fun cleanStoredList() {
        moviesLocalDataSource.clearList()
    }
}