package com.vmmontes.moviesapp.di

import android.content.Context
import com.vmmontes.api.RetrofitService
import com.vmmontes.api.client.MoviesClient
import com.vmmontes.api.client.MoviesClientImp
import com.vmmontes.moviesapp.MoviesApplication
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ApplicationModule(val exCurrencyApplication : MoviesApplication) {
    @Provides
    @Singleton
    fun provideContext() : Context = exCurrencyApplication.applicationContext

    @Provides
    @Singleton
    fun provideApiService(): RetrofitService = RetrofitService()
}