package com.vmmontes.moviesapp.di

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.domain.usecase.GetStoredMovieUseCase
import com.vmmontes.moviesapp.presentation.presenter.moviedetail.MovieDetailPresenter
import dagger.Module
import dagger.Provides

@Module
class DetailMovieModule {

    @Provides
    fun providesGetStoredMovieUseCase(moviesRepository: MoviesRepository): GetStoredMovieUseCase =
        GetStoredMovieUseCase(moviesRepository)

    @Provides
    fun provideMovieDetailPresenter(getStoredMovieUseCase: GetStoredMovieUseCase
    ): MovieDetailPresenter = MovieDetailPresenter(getStoredMovieUseCase)
}