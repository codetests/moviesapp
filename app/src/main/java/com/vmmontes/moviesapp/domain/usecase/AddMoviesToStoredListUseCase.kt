package com.vmmontes.moviesapp.domain.usecase

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.data.repository.MoviesRepositoryImp
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class AddMoviesToStoredListUseCase(val moviesRepository: MoviesRepository ) {

    fun execute(moviesList: MutableList<MovieDomainModel>) {
        moviesRepository.addMoviesIntoStoredList(moviesList)
    }
}