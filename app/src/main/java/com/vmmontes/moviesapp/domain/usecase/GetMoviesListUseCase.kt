package com.vmmontes.moviesapp.domain.usecase

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.data.repository.MoviesRepositoryImp
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class GetMoviesListUseCase(val moviesRepository: MoviesRepository) {

    fun execute(language: String, movieName: String, moviesResponse: MoviesResponse){
        val moviesList = moviesRepository.getMovies(language, movieName)
        if(moviesList.moviesList.size > 0) {
            moviesResponse.isSuccess(moviesList.moviesList)
        } else {
            when(moviesList.moviesListState) {
                ListMovieDomainModel.MoviesListState.SUCCESS -> moviesResponse.isEmpty()
                ListMovieDomainModel.MoviesListState.GENERIC_ERROR -> moviesResponse.error(moviesList.moviesListState)
                ListMovieDomainModel.MoviesListState.SERVER_ERROR -> moviesResponse.error(moviesList.moviesListState)
                ListMovieDomainModel.MoviesListState.MAX_PAGES -> moviesResponse.isLimitOfPagination()
            }
        }
    }

    interface MoviesResponse {
        fun isSuccess(moviesList: MutableList<MovieDomainModel>)
        fun isEmpty()
        fun isLimitOfPagination()
        fun error(error: ListMovieDomainModel.MoviesListState)
    }
}