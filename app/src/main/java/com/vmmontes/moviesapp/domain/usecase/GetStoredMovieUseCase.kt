package com.vmmontes.moviesapp.domain.usecase

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.data.repository.MoviesRepositoryImp
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class GetStoredMovieUseCase(val moviesRepository: MoviesRepository) {

    fun execute(id: Int): MovieDomainModel? = moviesRepository.getMovie(id)
}