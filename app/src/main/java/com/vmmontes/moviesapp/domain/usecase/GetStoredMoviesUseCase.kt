package com.vmmontes.moviesapp.domain.usecase

import com.vmmontes.moviesapp.data.repository.MoviesRepository
import com.vmmontes.moviesapp.data.repository.MoviesRepositoryImp
import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel

class GetStoredMoviesUseCase(val moviesRepository: MoviesRepository) {

    fun execute(): MutableList<MovieDomainModel> = moviesRepository.getStoredList()
}