package com.vmmontes.moviesapp.domain.usecase

class IsSameLastQuery {

    fun execute(currentQuery: String, lastQuery: String): Boolean = (currentQuery == lastQuery)
}