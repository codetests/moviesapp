package com.vmmontes.moviesapp.kernel.ui

import android.support.v4.app.Fragment
import com.vmmontes.moviesapp.MoviesApplication

abstract class BaseFragment : Fragment() {

    fun getApplication() : MoviesApplication {
        return activity!!.application as MoviesApplication
    }
}