package com.vmmontes.moviesapp.presentation.presenter.moviedetail

import com.vmmontes.moviesapp.domain.usecase.GetStoredMovieUseCase
import com.vmmontes.moviesapp.kernel.presenter.Presenter
import com.vmmontes.moviesapp.presentation.ui.moviedetail.MovieDetailView

class MovieDetailPresenter(val getStoredMovieUseCase: GetStoredMovieUseCase): Presenter<MovieDetailView>(){

    fun onViewCreated(id: Int) {
        val movie = getStoredMovieUseCase.execute(id)
        movie?.apply {
            view?.showMovie(this)
        }
    }
}