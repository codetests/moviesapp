package com.vmmontes.moviesapp.presentation.presenter.movieslist

import com.vmmontes.moviesapp.domain.model.ListMovieDomainModel
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.domain.usecase.*
import com.vmmontes.moviesapp.kernel.coroutines.backgroundContext
import com.vmmontes.moviesapp.kernel.coroutines.mainContext
import com.vmmontes.moviesapp.kernel.presenter.CoroutinesPresenter
import com.vmmontes.moviesapp.presentation.ui.movieslist.MoviesListView
import kotlinx.coroutines.launch

class MoviesListPresenter(
    val getMoviesListUseCase: GetMoviesListUseCase,
    val addMoviesToStoredListUseCase: AddMoviesToStoredListUseCase,
    val clearStoredMoviesUseCase: ClearStoredMoviesUseCase,
    val getStoredMoviesUseCase: GetStoredMoviesUseCase,
    val isSameLastQuery: IsSameLastQuery
): CoroutinesPresenter<MoviesListView>(), GetMoviesListUseCase.MoviesResponse{

    private fun resetInitialValues() {
        clearStoredMoviesUseCase.execute()
        view?.cleanList()
    }

    private fun getMovies(language: String, query: String) {
        launch(backgroundContext) {
            getMoviesListUseCase.execute(language, query, this@MoviesListPresenter)
        }
    }

    private fun showMoviesIntoList(moviesList: MutableList<MovieDomainModel>) {
        view?.hideLoading()
        view?.showMoviesIntoList(moviesList)
    }

    fun onQueryTextSubmit(language: String, query: String, lastQuery: String) {
        if (!isSameLastQuery.execute(query, lastQuery)) {
            resetInitialValues()
        }
        view?.hideKeyboard()
        view?.showLoading()
        view?.hideMessageText()
        view?.hideFindFilmInformation()
        getMovies(language, query)
    }

    fun showMoreItems(language: String, movie: String) {
        getMovies(language, movie)
    }

    override fun isSuccess(moviesList: MutableList<MovieDomainModel>) {
        addMoviesToStoredListUseCase.execute(moviesList)
        launch(mainContext) {
            showMoviesIntoList(moviesList)
        }
    }

    override fun isEmpty() {
        launch(mainContext) {
            view?.cleanList()
            view?.hideLoading()
            view?.showIsEmpty()
        }
    }

    override fun error(error: ListMovieDomainModel.MoviesListState) {
        launch(mainContext) {
            view?.cleanList()
            view?.hideLoading()
            view?.showError()
        }
    }

    override fun isLimitOfPagination() {
        launch(mainContext) {
            view?.hideLoading()
        }
    }

    fun onMovieListAction(movie: MovieDomainModel) {
        view?.openDetailMoview(movie.id)
    }
}