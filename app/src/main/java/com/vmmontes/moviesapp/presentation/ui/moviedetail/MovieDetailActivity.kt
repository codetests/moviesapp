package com.vmmontes.moviesapp.presentation.ui.moviedetail

import android.os.Bundle
import android.support.v4.content.ContextCompat
import com.vmmontes.moviesapp.R
import com.vmmontes.moviesapp.kernel.ui.BaseActivity
import com.vmmontes.moviesapp.utils.loadImageToURL
import kotlinx.android.synthetic.main.activity_with_transparent_toolbar.*

class MovieDetailActivity : BaseActivity() {

    companion object {
        const val ID_KEY = "ID_KEY"
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        window.statusBarColor = ContextCompat.getColor(this, R.color.colorPrimaryTransparent10Percent)
        setContentView(R.layout.activity_with_transparent_toolbar)
        configureActionBar()
        if (savedInstanceState == null) {
            intent.extras?.getInt(ID_KEY)?.also {id -> Int
                supportFragmentManager.beginTransaction()
                    .replace(R.id.view_frame, MovieDetailFragment.newInstance(id), MovieDetailFragment::class.java.name)
                    .commit()
            }
        }
    }

    private fun configureActionBar() {
        setSupportActionBar(toolbarParallax)
        showToolbarWithBackButton()
        supportActionBar?.let {
            title = ""
        }
    }

    fun setImg(url: String) {
        loadImageToURL(this, url, imgViewParallax)
    }

    fun setTitle(newTitle: String) {
        supportActionBar?.let {
            title = newTitle
        }
    }
}
