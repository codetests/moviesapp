package com.vmmontes.moviesapp.presentation.ui.moviedetail

import com.vmmontes.moviesapp.domain.model.MovieDomainModel

interface MovieDetailView {
    fun showMovie(movie: MovieDomainModel)
}