package com.vmmontes.moviesapp.presentation.ui.movieslist

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.vmmontes.api.kernel.BASE_IMG_URL
import com.vmmontes.moviesapp.R
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.utils.loadImageToURL
import kotlinx.android.synthetic.main.row_movie.view.*


class MoviesListAdapter(val context: Context,
                        var itemsDomain: MutableList<Any>,
                        val onClickListItem: (MovieDomainModel) -> Unit) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    inner class MoviewViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val title = view.movieTitle
        val img = view.imgThumbnail
        val adultImg = view.imageView18
        val rowMovie = view.cardViewMovie
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val viewHolder: RecyclerView.ViewHolder
        viewHolder = MoviewViewHolder(
            LayoutInflater.from(parent.context)
                .inflate(R.layout.row_movie, parent, false))
        return viewHolder
    }

    private fun getAdultImgVisibility(isAdult: Boolean): Int =
        if(isAdult) {
            View.VISIBLE
        } else {
            View.GONE
        }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val moview = itemsDomain[position] as MovieDomainModel
        val viewHolder = holder as MoviewViewHolder
        val posterURL = "$BASE_IMG_URL${moview.posterPath}"
        loadImageToURL(context, posterURL, viewHolder.img)
        viewHolder.title.text = moview.title
        viewHolder.adultImg.visibility = getAdultImgVisibility(moview.adult)
        viewHolder.rowMovie.setOnClickListener {
            onClickListItem(moview)
        }
    }

    override fun getItemCount(): Int {
        return itemsDomain.size
    }

    internal fun cleanItems() {
        this.itemsDomain = mutableListOf()
    }

    internal fun addMoreItems(moviesList: MutableList<MovieDomainModel>) {
        this.itemsDomain.addAll(moviesList)
    }
}
