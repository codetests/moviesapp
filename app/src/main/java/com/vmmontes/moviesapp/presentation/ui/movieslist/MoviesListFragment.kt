package com.vmmontes.moviesapp.presentation.ui.movieslist

import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.GridLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.SearchView
import android.view.*
import com.vmmontes.moviesapp.R
import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.kernel.ui.BaseFragment
import com.vmmontes.moviesapp.presentation.presenter.movieslist.MoviesListPresenter
import com.vmmontes.moviesapp.presentation.ui.moviedetail.MovieDetailActivity
import com.vmmontes.moviesapp.utils.hideKeyboardFrom
import kotlinx.android.synthetic.main.fragment_movies_list.*
import java.util.*
import javax.inject.Inject

class MoviesListFragment : BaseFragment(), MoviesListView, SearchView.OnQueryTextListener {
    private lateinit var mAdapter: MoviesListAdapter
    private var searchView: SearchView? = null
    private var isFirstTimeToSubmitQuery = false
    private var lastQuery = ""

    @Inject
    lateinit var presenter : MoviesListPresenter

    companion object {
        const val NUM_COLUMNS_OF_GRID = 2
        @JvmStatic
        fun newInstance() = MoviesListFragment()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setHasOptionsMenu(true)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_movies_list, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        getApplication().component.inject(this)
        presenter.onAttach(this)
        initMovieList()
    }

    private fun initMovieList(){
        mAdapter = MoviesListAdapter(context!!, mutableListOf()) { presenter.onMovieListAction(it) }
        val mLayoutManager = GridLayoutManager(context, NUM_COLUMNS_OF_GRID)
        rvMoviesList.layoutManager = mLayoutManager
        rvMoviesList.adapter = mAdapter
        addListScrollListener()
    }

    private fun addListScrollListener() {
        rvMoviesList.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (!recyclerView.canScrollVertically(1)) {
                    if(!isFirstTimeToSubmitQuery) {
                        presenter.showMoreItems(Locale.getDefault().language, lastQuery)
                    }
                    isFirstTimeToSubmitQuery = false
                }
            }
        })
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.onDetach()
    }

    override fun onCreateOptionsMenu(menu: Menu?, inflater: MenuInflater?) {
        inflater?.apply {inflate(R.menu.search_menu, menu)}
        val searchItem = menu?.findItem(R.id.action_search)
        searchView = searchItem?.actionView.apply {
            configureSearchView(searchItem)
        } as SearchView
        super.onCreateOptionsMenu(menu, inflater)
    }

    private fun configureSearchView(searchItem: MenuItem?): SearchView =
        (searchItem?.actionView as SearchView).apply {
            maxWidth = Integer.MAX_VALUE
            setOnQueryTextListener(this@MoviesListFragment)
        }


    override fun onQueryTextSubmit(query: String?): Boolean {
        query?.also {
            presenter.onQueryTextSubmit(Locale.getDefault().language, it, lastQuery)
            searchView?.onActionViewCollapsed()
            isFirstTimeToSubmitQuery = true
            lastQuery = it
        }
        return true
    }

    override fun onQueryTextChange(newText: String?): Boolean {
        //NO NEEDS IMPLEMENTATION
        return true
    }

    override fun showMoviesIntoList(moviesList: MutableList<MovieDomainModel>) {
        mAdapter.addMoreItems(moviesList)
        mAdapter.notifyDataSetChanged()
    }

    override fun hideKeyboard() {
        context?.let {context->
            view?.let {view->
                hideKeyboardFrom(context, view)
            }
        }
    }

    override fun cleanList() {
        mAdapter.cleanItems()
    }

    override fun showError() {
        textViewMessage.visibility = View.VISIBLE
        textViewMessage.text = resources.getString(R.string.error)
    }

    override fun showIsEmpty() {
        textViewMessage.visibility = View.VISIBLE
        textViewMessage.text = resources.getString(R.string.no_movies_founded).replace("$1", lastQuery)
    }

    override fun hideMessageText() {
        textViewMessage.visibility = View.GONE
    }

    override fun showLoading() {
        progresBarMoviesList.visibility = View.VISIBLE
    }

    override fun hideLoading() {
        progresBarMoviesList.visibility = View.GONE
    }

    override fun openDetailMoview(id: Int) {
        val intent = Intent(context!!, MovieDetailActivity::class.java)
        intent.putExtra(MovieDetailActivity.ID_KEY, id)
        activity!!.startActivity(intent)
    }

    override fun hideFindFilmInformation() {
        findFilmInformationLayout.visibility = View.GONE
    }
}
