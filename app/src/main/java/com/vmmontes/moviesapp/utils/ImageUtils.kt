package com.vmmontes.moviesapp.utils

import android.content.Context
import android.widget.ImageView
import com.bumptech.glide.Glide
import com.bumptech.glide.Priority
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.RequestOptions
import com.vmmontes.moviesapp.R


fun loadImageToURL(context: Context, url: String, imageView: ImageView) {
    val options = RequestOptions()
            .centerInside()
            .placeholder(R.drawable.no_poster_avaliable)
            .error(R.drawable.no_poster_avaliable)
            .diskCacheStrategy(DiskCacheStrategy.ALL)
            .priority(Priority.HIGH)

    Glide.with(context)
            .load(url)
            .apply(options)
            .into(imageView)

}
