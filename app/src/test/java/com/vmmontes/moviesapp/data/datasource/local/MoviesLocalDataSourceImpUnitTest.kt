package com.vmmontes.moviesapp.data.datasource.local

import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import org.junit.Assert
import org.junit.Before
import org.junit.Test

class MoviesLocalDataSourceImpUnitTest {

    private lateinit var moviesLocalDataSource: MoviesLocalDataSource

    @Before
    fun setup() {
        moviesLocalDataSource = MoviesLocalDataSourceImp()
    }

    @Test
    fun `should return list with three items`(){
        // arrange
        val list: MutableList<MovieDomainModel> = mutableListOf()
        list.add(MovieDomainModel(1, "url", false, "date",
            "title", "overview", 5.0, 2, "original"))
        list.add(MovieDomainModel(2, "url", false, "date",
            "title", "overview", 5.0, 2, "original"))
        list.add(MovieDomainModel(3, "url", false, "date",
            "title", "overview", 5.0, 2, "original"))

        //act
        moviesLocalDataSource.addall(list)

        // assert
        Assert.assertTrue(moviesLocalDataSource.getList().size == 3)
    }

    @Test
    fun `should return list with zero items`(){
        // arrange
        val list: MutableList<MovieDomainModel> = mutableListOf()
        list.add(MovieDomainModel(1, "url", false, "date",
            "title", "overview", 5.0, 2, "original"))
        list.add(MovieDomainModel(2, "url", false, "date",
            "title", "overview", 5.0, 2, "original"))
        list.add(MovieDomainModel(3, "url", false, "date",
            "title", "overview", 5.0, 2, "original"))

        //act
        moviesLocalDataSource.addall(list)
        moviesLocalDataSource.clearList()

        // assert
        Assert.assertTrue(moviesLocalDataSource.getList().size == 0)
    }

    @Test
    fun `should get movie by id`(){
        // arrange
        val list: MutableList<MovieDomainModel> = mutableListOf()
        val movieToFind = MovieDomainModel(1, "url", false, "date",
            "title", "overview", 5.0, 2, "original")
        val movietwo = MovieDomainModel(2, "url", false, "date",
            "title", "overview", 5.0, 2, "original")
        val moviethree = MovieDomainModel(3, "url", false, "date",
            "title", "overview", 5.0, 2, "original")
        list.add(movieToFind)
        list.add(movietwo)
        list.add(moviethree)

        //act
        moviesLocalDataSource.addall(list)
        val movie = moviesLocalDataSource.getMovie(1)

        // assert
        Assert.assertTrue(movie == movieToFind)
        Assert.assertFalse(movie == movietwo)
        Assert.assertFalse(movie == moviethree)
    }
}
