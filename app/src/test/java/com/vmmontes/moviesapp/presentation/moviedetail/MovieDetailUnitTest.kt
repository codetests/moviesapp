package com.vmmontes.moviesapp.presentation.moviedetail

import com.vmmontes.moviesapp.domain.model.MovieDomainModel
import com.vmmontes.moviesapp.domain.usecase.*
import com.vmmontes.moviesapp.kernel.coroutines.UncaughtCoRoutineExceptionHandler
import com.vmmontes.moviesapp.kernel.coroutines.backgroundContext
import com.vmmontes.moviesapp.kernel.coroutines.mainContext
import com.vmmontes.moviesapp.presentation.presenter.moviedetail.MovieDetailPresenter
import com.vmmontes.moviesapp.presentation.presenter.movieslist.MoviesListPresenter
import com.vmmontes.moviesapp.presentation.ui.moviedetail.MovieDetailView
import com.vmmontes.moviesapp.presentation.ui.movieslist.MoviesListView
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Dispatchers.Unconfined
import org.junit.Assert
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito

class MovieDetailUnitTest {

    private lateinit var view: MovieDetailView
    private lateinit var getStoredMovieUseCase: GetStoredMovieUseCase
    private lateinit var presenter: MovieDetailPresenter

    @Before
    fun setup() {
        getStoredMovieUseCase = Mockito.mock(GetStoredMovieUseCase::class.java)
        presenter = MovieDetailPresenter(getStoredMovieUseCase)
        view = Mockito.mock(MovieDetailView::class.java)
    }

    @Test
    fun `should call list movies`(){
        // arrange
        val mockMovie = MovieDomainModel(1, "url", true, "date",
            "title", "overview", 6.0, 2, "original")
        presenter.onAttach(view)
        Mockito.`when`(getStoredMovieUseCase.execute(1)).thenReturn(mockMovie)

        // act
        presenter.onViewCreated(1)

        // assert
        Mockito.verify(getStoredMovieUseCase, Mockito.times(1)).execute(1)
        Mockito.verify(view, Mockito.times(1)).showMovie(mockMovie)
        Assert.assertTrue(getStoredMovieUseCase.execute(1) == mockMovie)
    }
}
